const utils = require('./utils');

module.exports = {
  updateData: (payload) => {
    const appendix1 = payload;
    const strLevels = Object.keys(appendix1);
    const numLevels = strLevels.map((l) => parseInt(l));
    const descNumLevels = numLevels.sort((a, b) => b - a);
    //    [0, 1, 2] =>   [0, 1]
    const lastLevel = descNumLevels.shift(); //remove last element

    const transformedData = utils.transformData(
      descNumLevels,
      appendix1,
      lastLevel,
    );
    return transformedData;
  },
};
