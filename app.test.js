const app = require('./app.js'); // Import Server/Application
const mock = require('./mock.js');

// Start application before running the test case
beforeAll((done) => {
  app.events.on('start', () => {
    done();
  });
});

// Stop application after running the test case
afterAll((done) => {
  app.events.on('stop', () => {
    done();
  });
  app.stop();
});

test('should successful transform data', async function () {
  const options = {
    method: 'PUT',
    url: '/transform',
    payload: JSON.stringify(mock.transformInput),
  };

  const data = await app.inject(options);
  expect(data.statusCode).toBe(200);
  expect(data.result).toEqual(mock.transformOutput);
});
