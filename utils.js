module.exports = {
  updateParentLevel: (appendix2, parentLevelList) => {
    return appendix2.reduce((updatedParentLevelList, obj) => {
      const parentId = obj['parent_id'];
      return parentLevelList.map((parentObj) => {
        if (parentId == parentObj['id']) {
          parentObj['children'] = [...parentObj['children'], obj];
        }
        return parentObj;
      });
    }, []);
  },
  transformData: (descNumLevels, appendix1, lastLevel) => {
    const lastLevelList = appendix1[lastLevel];
    return descNumLevels.reduce((appendix2, parentLevel) => {
      const parentStrLevel = parentLevel.toString();
      const parentLevelList = appendix1[parentStrLevel];

      return module.exports.updateParentLevel(appendix2, parentLevelList);
    }, lastLevelList);
  },
};
