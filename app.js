const Hapi = require('@hapi/hapi');
const utils = require('./utils');
const api = require('./api');

const server = Hapi.server({
  port: 3000,
  host: 'localhost',
});

server.route({
  method: 'PUT',
  path: '/transform',
  handler: (request, h) => api.updateData(request.payload),
});

const init = async () => {
  await server.start();
  console.log('Server running on %s', server.info.uri);
};

process.on('unhandledRejection', (err) => {
  console.log(err);
  process.exit(1);
});

init();

module.exports = server;
